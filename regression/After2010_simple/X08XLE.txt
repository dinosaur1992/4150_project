
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
      Min        1Q    Median        3Q       Max 
-0.057105 -0.004917 -0.000184  0.004920  0.037204 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept) -4.783e-05  2.110e-04  -0.227    0.821    
MKT          1.175e-02  2.595e-04  45.289  < 2e-16 ***
SMB         -3.253e-03  6.794e-04  -4.788 1.83e-06 ***
HML          5.191e-03  7.296e-04   7.115 1.67e-12 ***
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.008518 on 1631 degrees of freedom
Multiple R-squared:  0.6569,	Adjusted R-squared:  0.6563 
F-statistic:  1041 on 3 and 1631 DF,  p-value: < 2.2e-16

