
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
      Min        1Q    Median        3Q       Max 
-0.030739 -0.004270  0.000079  0.003899  0.063743 

Coefficients:
             Estimate Std. Error t value Pr(>|t|)    
(Intercept) 0.0001589  0.0001690   0.940    0.347    
MKT         0.0126536  0.0002079  60.864  < 2e-16 ***
SMB         0.0034224  0.0005443   6.288 4.13e-10 ***
HML         0.0003367  0.0005845   0.576    0.565    
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.006825 on 1631 degrees of freedom
Multiple R-squared:  0.7277,	Adjusted R-squared:  0.7272 
F-statistic:  1453 on 3 and 1631 DF,  p-value: < 2.2e-16

