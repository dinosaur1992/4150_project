
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
      Min        1Q    Median        3Q       Max 
-0.037713 -0.004259  0.000112  0.004542  0.063003 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept)  0.0002599  0.0002011   1.292    0.196    
MKT          0.0095740  0.0002474  38.703   <2e-16 ***
SMB         -0.0010086  0.0006477  -1.557    0.120    
HML         -0.0013299  0.0006955  -1.912    0.056 .  
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.008121 on 1631 degrees of freedom
Multiple R-squared:  0.5394,	Adjusted R-squared:  0.5385 
F-statistic: 636.6 on 3 and 1631 DF,  p-value: < 2.2e-16

