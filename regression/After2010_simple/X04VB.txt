
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
      Min        1Q    Median        3Q       Max 
-0.026872 -0.003542 -0.000096  0.003331  0.046735 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept)  0.0001312  0.0001439   0.911    0.362    
MKT          0.0126737  0.0001770  71.600  < 2e-16 ***
SMB          0.0023037  0.0004634   4.971 7.36e-07 ***
HML         -0.0026248  0.0004977  -5.274 1.51e-07 ***
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.005811 on 1631 degrees of freedom
Multiple R-squared:  0.7845,	Adjusted R-squared:  0.7841 
F-statistic:  1979 on 3 and 1631 DF,  p-value: < 2.2e-16

