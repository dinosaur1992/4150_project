
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
      Min        1Q    Median        3Q       Max 
-0.031749 -0.003627  0.000017  0.003590  0.035064 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept)  8.679e-05  1.475e-04   0.588  0.55637    
MKT          1.243e-02  1.814e-04  68.488  < 2e-16 ***
SMB         -1.227e-03  4.751e-04  -2.584  0.00986 ** 
HML         -7.586e-04  5.102e-04  -1.487  0.13722    
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.005957 on 1631 degrees of freedom
Multiple R-squared:  0.7875,	Adjusted R-squared:  0.7871 
F-statistic:  2015 on 3 and 1631 DF,  p-value: < 2.2e-16

