
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
      Min        1Q    Median        3Q       Max 
-0.035572 -0.007998 -0.000627  0.008423  0.041354 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept) -0.0046731  0.0006762  -6.911 1.95e-11 ***
MKT          0.0076185  0.0005318  14.327  < 2e-16 ***
SMB         -0.0012195  0.0013330  -0.915 0.360833    
HML         -0.0054775  0.0014579  -3.757 0.000198 ***
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.01343 on 393 degrees of freedom
Multiple R-squared:  0.5302,	Adjusted R-squared:  0.5266 
F-statistic: 147.8 on 3 and 393 DF,  p-value: < 2.2e-16

