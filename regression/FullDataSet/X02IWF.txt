
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
      Min        1Q    Median        3Q       Max 
-0.022773  0.000360  0.002373  0.003585  0.017878 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept) -0.0028071  0.0001361 -20.619  < 2e-16 ***
MKT          0.0044760  0.0001506  29.722  < 2e-16 ***
SMB         -0.0011275  0.0003849  -2.930  0.00343 ** 
HML         -0.0029154  0.0004169  -6.994 3.46e-12 ***
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.00668 on 2405 degrees of freedom
Multiple R-squared:  0.3654,	Adjusted R-squared:  0.3647 
F-statistic: 461.7 on 3 and 2405 DF,  p-value: < 2.2e-16

