
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
      Min        1Q    Median        3Q       Max 
-0.027827 -0.000914  0.002031  0.004067  0.038727 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept) -0.0028225  0.0001472 -19.172  < 2e-16 ***
MKT          0.0058279  0.0001629  35.787  < 2e-16 ***
SMB          0.0019453  0.0004162   4.674 3.12e-06 ***
HML         -0.0005309  0.0004508  -1.178    0.239    
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.007224 on 2405 degrees of freedom
Multiple R-squared:   0.41,	Adjusted R-squared:  0.4092 
F-statistic:   557 on 3 and 2405 DF,  p-value: < 2.2e-16

