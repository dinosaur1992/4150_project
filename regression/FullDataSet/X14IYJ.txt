
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
      Min        1Q    Median        3Q       Max 
-0.023458 -0.000534  0.002177  0.003869  0.033576 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept) -0.0028132  0.0001407 -19.999   <2e-16 ***
MKT          0.0049945  0.0001556  32.099   <2e-16 ***
SMB         -0.0004765  0.0003976  -1.198    0.231    
HML         -0.0004641  0.0004307  -1.078    0.281    
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.006902 on 2405 degrees of freedom
Multiple R-squared:  0.4008,	Adjusted R-squared:  0.4001 
F-statistic: 536.3 on 3 and 2405 DF,  p-value: < 2.2e-16

