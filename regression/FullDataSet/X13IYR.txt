
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
      Min        1Q    Median        3Q       Max 
-0.062770 -0.002605  0.001936  0.004841  0.064579 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept) -2.872e-03  1.952e-04 -14.713  < 2e-16 ***
MKT          5.651e-03  2.159e-04  26.173  < 2e-16 ***
SMB         -3.042e-05  5.518e-04  -0.055  0.95603    
HML          1.723e-03  5.977e-04   2.882  0.00399 ** 
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.009578 on 2405 degrees of freedom
Multiple R-squared:  0.3163,	Adjusted R-squared:  0.3154 
F-statistic: 370.8 on 3 and 2405 DF,  p-value: < 2.2e-16

