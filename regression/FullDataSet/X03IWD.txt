
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
       Min         1Q     Median         3Q        Max 
-0.0250506  0.0001529  0.0023656  0.0036631  0.0276675 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept) -0.0028441  0.0001402 -20.287  < 2e-16 ***
MKT          0.0045243  0.0001551  29.174  < 2e-16 ***
SMB         -0.0015875  0.0003963  -4.005 6.38e-05 ***
HML          0.0006610  0.0004293   1.540    0.124    
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.006879 on 2405 degrees of freedom
Multiple R-squared:  0.3931,	Adjusted R-squared:  0.3924 
F-statistic: 519.3 on 3 and 2405 DF,  p-value: < 2.2e-16

