
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
      Min        1Q    Median        3Q       Max 
-0.053403 -0.002182  0.001810  0.004401  0.044733 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept) -0.0029168  0.0001603 -18.197  < 2e-16 ***
MKT          0.0058065  0.0001773  32.749  < 2e-16 ***
SMB         -0.0020613  0.0004531  -4.549 5.65e-06 ***
HML         -0.0019286  0.0004908  -3.930 8.75e-05 ***
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.007865 on 2405 degrees of freedom
Multiple R-squared:  0.4309,	Adjusted R-squared:  0.4302 
F-statistic: 607.1 on 3 and 2405 DF,  p-value: < 2.2e-16

