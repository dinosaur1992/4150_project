
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
      Min        1Q    Median        3Q       Max 
-0.035247 -0.001378  0.001480  0.003592  0.027927 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept) -1.988e-03  1.298e-04 -15.320   <2e-16 ***
MKT          3.465e-03  1.419e-04  24.413   <2e-16 ***
SMB         -1.386e-04  3.621e-04  -0.383    0.702    
HML         -8.275e-05  3.898e-04  -0.212    0.832    
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.006223 on 2298 degrees of freedom
  (107 observations deleted due to missingness)
Multiple R-squared:  0.2878,	Adjusted R-squared:  0.2869 
F-statistic: 309.6 on 3 and 2298 DF,  p-value: < 2.2e-16

