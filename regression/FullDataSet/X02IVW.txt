
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
       Min         1Q     Median         3Q        Max 
-0.0230033  0.0002517  0.0023688  0.0035604  0.0277486 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept) -0.0028037  0.0001367 -20.502  < 2e-16 ***
MKT          0.0043166  0.0001513  28.537  < 2e-16 ***
SMB         -0.0014659  0.0003866  -3.792 0.000153 ***
HML         -0.0026753  0.0004187  -6.389 1.99e-10 ***
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.00671 on 2405 degrees of freedom
Multiple R-squared:  0.3566,	Adjusted R-squared:  0.3558 
F-statistic: 444.2 on 3 and 2405 DF,  p-value: < 2.2e-16

