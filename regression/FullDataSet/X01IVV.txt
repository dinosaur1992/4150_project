
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
       Min         1Q     Median         3Q        Max 
-0.0242191  0.0003565  0.0024172  0.0035557  0.0272713 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept) -0.0028235  0.0001377 -20.509  < 2e-16 ***
MKT          0.0043697  0.0001523  28.694  < 2e-16 ***
SMB         -0.0016310  0.0003892  -4.191 2.88e-05 ***
HML         -0.0010009  0.0004215  -2.374   0.0177 *  
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.006755 on 2405 degrees of freedom
Multiple R-squared:  0.3728,	Adjusted R-squared:  0.372 
F-statistic: 476.5 on 3 and 2405 DF,  p-value: < 2.2e-16 

