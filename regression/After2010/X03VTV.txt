
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
       Min         1Q     Median         3Q        Max 
-0.0114023 -0.0009976 -0.0000096  0.0009090  0.0093580 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept)  6.684e-05  4.181e-05   1.599    0.110    
MKT          3.996e-03  5.142e-05  77.704   <2e-16 ***
SMB         -2.283e-03  1.346e-04 -16.961   <2e-16 ***
HML          6.597e-05  1.446e-04   0.456    0.648    
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.001688 on 1631 degrees of freedom
Multiple R-squared:  0.8514,	Adjusted R-squared:  0.8511 
F-statistic:  3115 on 3 and 1631 DF,  p-value: < 2.2e-16

