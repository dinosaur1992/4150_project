
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
       Min         1Q     Median         3Q        Max 
-0.0092000 -0.0013613 -0.0000748  0.0014795  0.0135565 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept)  5.287e-05  5.643e-05   0.937   0.3490    
MKT          4.929e-03  6.942e-05  71.010  < 2e-16 ***
SMB         -1.197e-03  1.817e-04  -6.587 6.03e-11 ***
HML         -4.123e-04  1.952e-04  -2.112   0.0348 *  
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.002279 on 1631 degrees of freedom
Multiple R-squared:  0.8065,	Adjusted R-squared:  0.8061 
F-statistic:  2266 on 3 and 1631 DF,  p-value: < 2.2e-16

