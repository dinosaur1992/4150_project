
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
       Min         1Q     Median         3Q        Max 
-0.0172898 -0.0017912  0.0001272  0.0019607  0.0131519 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept)  1.369e-04  7.802e-05   1.755   0.0795 .  
MKT          2.164e-03  9.596e-05  22.550  < 2e-16 ***
SMB         -1.717e-03  2.512e-04  -6.836 1.15e-11 ***
HML          2.791e-04  2.698e-04   1.034   0.3011    
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.00315 on 1631 degrees of freedom
Multiple R-squared:  0.3494,	Adjusted R-squared:  0.3482 
F-statistic:   292 on 3 and 1631 DF,  p-value: < 2.2e-16

