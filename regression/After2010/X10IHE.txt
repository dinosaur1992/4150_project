
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
       Min         1Q     Median         3Q        Max 
-0.0173604 -0.0017924  0.0000041  0.0018180  0.0144507 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept)  9.917e-05  7.880e-05   1.258    0.208    
MKT          4.084e-03  9.693e-05  42.136  < 2e-16 ***
SMB         -1.405e-03  2.538e-04  -5.536  3.6e-08 ***
HML         -4.936e-03  2.725e-04 -18.110  < 2e-16 ***
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.003182 on 1631 degrees of freedom
Multiple R-squared:  0.5879,	Adjusted R-squared:  0.5872 
F-statistic: 775.7 on 3 and 1631 DF,  p-value: < 2.2e-16

