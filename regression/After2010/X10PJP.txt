
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
       Min         1Q     Median         3Q        Max 
-0.0183855 -0.0017614  0.0000243  0.0019338  0.0149961 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept)  1.771e-04  8.355e-05   2.119   0.0342 *  
MKT          4.251e-03  1.028e-04  41.364  < 2e-16 ***
SMB         -1.406e-03  2.691e-04  -5.227 1.94e-07 ***
HML         -5.499e-03  2.890e-04 -19.030  < 2e-16 ***
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.003374 on 1631 degrees of freedom
Multiple R-squared:  0.5794,	Adjusted R-squared:  0.5786 
F-statistic: 748.8 on 3 and 1631 DF,  p-value: < 2.2e-16

