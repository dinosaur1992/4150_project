
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
      Min        1Q    Median        3Q       Max 
-0.018958 -0.001526 -0.000075  0.001504  0.017124 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept)  1.636e-05  6.974e-05   0.235    0.815    
MKT          4.813e-03  8.579e-05  56.105  < 2e-16 ***
SMB         -1.903e-03  2.246e-04  -8.475  < 2e-16 ***
HML          1.456e-03  2.412e-04   6.038 1.93e-09 ***
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.002816 on 1631 degrees of freedom
Multiple R-squared:  0.7477,	Adjusted R-squared:  0.7473 
F-statistic:  1612 on 3 and 1631 DF,  p-value: < 2.2e-16

