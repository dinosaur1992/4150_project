
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
       Min         1Q     Median         3Q        Max 
-0.0080704 -0.0009911  0.0000055  0.0009561  0.0089619 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept)  4.840e-05  4.002e-05   1.209    0.227    
MKT          4.282e-03  4.923e-05  86.993   <2e-16 ***
SMB         -1.942e-03  1.289e-04 -15.071   <2e-16 ***
HML         -2.892e-03  1.384e-04 -20.891   <2e-16 ***
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.001616 on 1631 degrees of freedom
Multiple R-squared:  0.8633,	Adjusted R-squared:  0.8631 
F-statistic:  3434 on 3 and 1631 DF,  p-value: < 2.2e-16

