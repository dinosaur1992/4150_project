
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
       Min         1Q     Median         3Q        Max 
-0.0074377 -0.0009927  0.0000305  0.0009990  0.0100831 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept)  5.675e-05  4.100e-05   1.384    0.166    
MKT          3.991e-03  5.043e-05  79.149   <2e-16 ***
SMB         -2.285e-03  1.320e-04 -17.306   <2e-16 ***
HML         -2.910e-03  1.418e-04 -20.526   <2e-16 ***
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.001655 on 1631 degrees of freedom
Multiple R-squared:  0.8451,	Adjusted R-squared:  0.8448 
F-statistic:  2965 on 3 and 1631 DF,  p-value: < 2.2e-16

