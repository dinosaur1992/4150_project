
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
       Min         1Q     Median         3Q        Max 
-0.0112781 -0.0010046  0.0000078  0.0009792  0.0093575 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept)  5.923e-05  4.290e-05   1.381   0.1675    
MKT          4.165e-03  5.276e-05  78.939   <2e-16 ***
SMB         -2.033e-03  1.381e-04 -14.718   <2e-16 ***
HML          2.975e-04  1.484e-04   2.006   0.0451 *  
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.001732 on 1631 degrees of freedom
Multiple R-squared:  0.8527,	Adjusted R-squared:  0.8524 
F-statistic:  3147 on 3 and 1631 DF,  p-value: < 2.2e-16

