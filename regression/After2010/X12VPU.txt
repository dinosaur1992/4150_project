
Call:
lm(formula = r ~ MKT + SMB + HML, data = model_data)

Residuals:
       Min         1Q     Median         3Q        Max 
-0.0169096 -0.0017507  0.0000781  0.0019204  0.0130228 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept)  1.354e-04  7.557e-05   1.791   0.0734 .  
MKT          2.356e-03  9.296e-05  25.342  < 2e-16 ***
SMB         -1.460e-03  2.434e-04  -6.000 2.42e-09 ***
HML          2.400e-04  2.614e-04   0.918   0.3587    
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.003052 on 1631 degrees of freedom
Multiple R-squared:  0.3868,	Adjusted R-squared:  0.3857 
F-statistic: 342.9 on 3 and 1631 DF,  p-value: < 2.2e-16

