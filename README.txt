IEOR E4150 - Project 
Team: Busy Cauchy
Author: Yuchen Zhang, Xinzhuo Jiang, and Ya-tzu (Audrey) Lee

===================================== Introduction ======================================
This package contains files for stock analysis application designed by Busy Cauchy team. 
We host our application at https://dinosaur.shinyapps.io/4150_project/. 
Or it can also be run stand-alone from your local computer. 
This application will analyze the basic statistics for any given ticker during a time 
frame, including but not limited to plotting histograms of its logReturn, 
performing regression vs time, analyzing confidence intervals for its mean and variance, 
performing regression against another ticker, and predicting expected return using 
Fama-French 3-factor model.   

================================== Package Information ==================================
This package contains all the files required to run our shiny app. 
server.R -- Shiny server file
ui.R -- Shiny UI
data_analysis.R -- Contains data analytics methods
retrieve_stock_price.R -- Contains function that retrieves stock price data dynamically 
Global_3_Factors_Daily.csv -- Fama-French Parameters table
www/ -- Contains CSS and JS file for shiny app
Project.Rproj -- R project file
deployment.sh -- Deployment script to deploy this app to Shiny server

================================ Instructions to Run App ================================
1. Load entire folder or Project.Rproj to RStudio 
2. R-packages required to run this app: shiny, quantmod, gdata
3. Run Server.R as Shiny app
4. Please maximize app window for best visual quality. 

======================================== Contacts =======================================
Please contact us via the following Email addresses if you have any questions. 

Yuchen Zhang: yz3022@columbia.edu
Xinzhuo Jiang: xj2193@columbia.edu
Audrey Lee: yl3584@columbia.edu
